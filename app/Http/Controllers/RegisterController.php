<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function index()
    {
        $user = User::sortable()->paginate(2);
        return view('register', ['user' => $user]);
    }

    public function store (Request $request)
    {
        $pesan = [
            'required' => ':attribute wajib diisi !',
            'min' => ':attribute harus diisi minimal :min karakter !',
            'max' => ':attribute harus diisi maksimal :max karakter !',
            'numeric' => ':attribute harus diisi angka !',
        ];

        $this->validate($request, [
            'nama' => 'required|max:255',
            'jenis_kelamin'=> 'required|max:255',
            'hobi'=> 'required|max:255',
            'telp'=> 'required|numeric',
            'username'=> 'required|max:10',
            'email' => 'required|unique:users',
            'password' => 'required|min:7',
        ], $pesan);


        $input = $request->all();
        $input['hobi'] = json_encode($request->input('hobi'));
        // dd($input['hobi'] );
        $input['password'] = Hash::make($request->input('password'));
        // dd($input['hobi']);
        User::create($input);
        $user = User::sortable()->paginate(2);
        return view('register',['data' => $request, 'user'=>$user])->with('success', 'Data user berhasil ditambahkan');
        // return redirect()->back()->with(['data' => $request]);;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect('/register')->with('success', 'Data user berhasil dihapus');
    }

    public function search(Request $request)
    {
            $search = $request->cari;
            $user = DB::table('users')
                ->where('nama', 'like', "%" . $search . "%")
                ->paginate(2);
            return view('register',['user' => $user]);


    }
}