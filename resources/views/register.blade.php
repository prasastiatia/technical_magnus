<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>
	Halaman Daftar
	</title>
	<!-- Favicon -->
	
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<!-- Icons -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/argon-dashboard-react/1.2.0/assets/plugins/nucleo/css/nucleo.min.css" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" rel="stylesheet" />
	<!-- CSS Files -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/argon-dashboard-react/1.2.0/assets/css/argon-dashboard-react.min.css" rel="stylesheet" />

    <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>
    
</head>


<body class="bg-default">
    <br>
	<div class="main-content">
		<!-- Page content -->
		<div class="container-fluid content">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card">
                        <div class="card-header">
                            <h3>Register</h3>
                        </div>
                        <div class="card-body">
                            {{-- menampilkan error validasi --}}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(session()->get('success'))
                            <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('success') }}  
                            </div><br />
                            @endif
                            <br/>
                        <form name ="register" id="register" class="register form1" method="post" action="{{ route('tambah_user') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-12">
                                    <label class="form-control-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" value="" >
                                    </div>
                                </div>

                                <div class="row" id="jenis_kelamin">
                                    <label class="form-control-label col-md-4">Jenis Kelamin </label>
                                    
                                    <div class="custom-control custom-radio col-4 col-md-2">
                                        <input type="radio" class="custom-control-input" id="laki_laki" name="jenis_kelamin" value="Laki-laki">
                                        <label class="custom-control-label" for="laki_laki">Laki-laki</label>
                                    </div>
                                    <div class="custom-control custom-radio col-4 col-md-2">
                                        <input type="radio" class="custom-control-input" id="perempuan" name="jenis_kelamin" value="Perempuan">
                                        <label class="custom-control-label" for="perempuan">Perempuan</label>
                                    </div>
                
                                </div>

                                <div class="row" id="hobi">
                                    <label class="form-control-label col-md-4">Hobi </label>
                                    
                                    <div class="custom-control custom-checkbox col-4 col-md-2">
                                        <input class="custom-control-input" type="checkbox" name="hobi[]" value="coding" id="coding">
                                        <label class="custom-control-label" for="coding">Coding</label>
                                    </div>
                                    <div class="custom-control custom-checkbox col-4 col-md-2">
                                        <input class="custom-control-input" type="checkbox" name="hobi[]" value="badminton" id="badminton">
                                        <label class="custom-control-label" for="badminton">Badminton</label>
                                    </div>
                                    <div class="custom-control custom-checkbox col-4 col-md-2">
                                        <input class="custom-control-input" type="checkbox" name="hobi[]" value="membaca" id="membaca">
                                        <label class="custom-control-label" for="membaca">Membaca</label>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-control-label">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" value="" >
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-control-label">Telp</label>
                                        <input type="text" class="form-control" id="telp" name="telp" value="">
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-control-label">Username</label>
                                        <input type="text" class="form-control" id="username" name="username" value="" >
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-control-label">Password</label> 
                                        <input type="password" class="form-control" id="password" name="password" value="" >
                                        
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary my-4">Daftar</button>
                                    <button id="reset" type="reset" class="btn btn-warning">Reset</button>
                                </div>
                               
                            </form>
                            <br>
                            <p>Cari Data Pengguna :</p>
                            <form action="/register/cari" method="GET">
                                <input type="text" name="cari" placeholder="Cari Pengguna .." value="{{ old('cari') }}">
                                <input type="submit" value="CARI">
                            </form>
                            <br>
                            <table class="table table-bordered table-hover table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('Nama')</th>
                                        <th>@sortablelink('Jenis Kelamin')</th>
                                        <th>Hobi</th>
                                        <th>Email</th>
                                        <th>Telp</th>
                                        <th>Username</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user as $u)
                                    <tr>
                                        <td>{{ $u->nama}}</td>
                                        <td>{{ $u->jenis_kelamin}}<</td>
                                        <td>{{ $u->hobi}}<</td>
                                        <td>{{ $u->email}}</td>
                                        <td>{{ $u->telp}}</td>
                                        <td>{{ $u->username}}</td>
                                        <td>
                                            <form action="{{ route('hapus_user', $u->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" type="submit">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br/>
                            Halaman : {{ $user->currentPage() }} <br/>
                            Jumlah Data : {{ $user->total() }} <br/>
                            Data Per Halaman : {{ $user->perPage() }} <br/> <br>
                        
                        
                            {{ $user->links() }}
                            <br>
                        </div>
                    </div>
                    <br>
                   
                </div>
            </div>  
        </div>
	</div>
    <br />
	<!--   Core   -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.1/js/bootstrap.bundle.min.js"></script>
	<!--   Optional JS   -->
	<!--   Argon JS   -->
	<script src="https://cdn.trackjs.com/agent/v3/latest/t.js'); ?>"></script>
	<script type="text/javascript">
		window.TrackJS &&
			TrackJS.install({
				token: "ee6fab19c5a04ac1a32a645abde4613a",
				application: "argon-dashboard-free"
			});

            $().alert('close')
            $('#reset').click(function(){
                document.getElementById("register").reset();
				});
	</script>
    
        
        
</body>

</html>